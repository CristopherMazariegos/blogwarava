import React, { Component } from 'react';
import Modal from 'react-modal';
import './MySmallModal.css';
import api from '../data/api.js';


export class MySmallModal extends Component {
    constructor() {
    super();
    this.state = { modalIsOpen: false,
    comments:[]};
    }
  
    openModal = () => {
        this.setState({modalIsOpen: true});
    }

    closeModal = () => {
        this.setState({modalIsOpen: false});
    }

    handleModalCloseRequest = () => {
        this.setState({modalIsOpen: false});
    }

    componentDidMount(){
        api.posts.getComments(this.props.postpadre)
          .then(res=> {
              this.setState({
                  comments:res
              });
          })
          .catch(error=> {
              console.log(error)
          })
    }

    render() {   
        const commentList = this.state.comments.map(function(comment){
            return(
                <div className="card-body" key={comment.id}>
                    <p>{comment.email} </p>
                    <div className="form-control" >
                        <p className="PPP">{comment.name}
                        </p>
                        <br></br> 
                        {comment.body}
                    </div>
                </div>
            );
        });

    return (
        <div>
            <button type="button" className="btn" onClick={this.openModal}>Ver Artículos</button>
            <Modal
                className="Modal__Bootstrap modal-dialog"
                closeTimeoutMS={150}
                isOpen={this.state.modalIsOpen}
                onRequestClose={this.handleModalCloseRequest}>
                    <div className="modal-content">
                      <div className="modal-header">
                          <h4 className="modal-title">Artículo</h4>
                          <button type="button" className="close" onClick={this.handleModalCloseRequest}>
                          <span aria-hidden="true">&times;</span>
                          <span className="sr-only">Close</span>
                          </button>
                              </div>
                                  <div className="modal-body">
                                        <div className="card2">
                                            <div>
                                                <div className="titulo1">{this.props.postTitulo}</div>
                                                <div className="cuerpopublicacion">{this.props.postBody}</div>
                                                <img src="https://yt3.ggpht.com/a-/ACSszfFPy2UBWsUaiPHq-i3vsYQSs2mNATaow9U3mw=s900-mo-c-c0xffffffff-rj-k-no" className="imagen"></img>
                                                <br/><br/>
                                            </div>
                                        </div>
                                        {commentList}
                                  <input
                                      type="text"
                                      name="comentario"
                                      className="form-control"
                                      placeholder="Agrega un comentario"
                                  />
                          <button type="button" className="btn">Guardar Comentario</button>
                                  </div>
                                        <div className="modal-footer">
                                          <button type="button" className="btn" onClick={this.handleModalCloseRequest}>Cerrar</button>
                                        </div>
                    </div>
            </Modal>
            <br></br>    
      </div>
    );
  }
}
export default MySmallModal;