import React, { Component } from 'react';
import MySmallModal from './MySmallModal';
import './Posts.css';
import Loader from 'react-loader-spinner'

    export class Posts extends Component {
    
        renderPosts() {
            return this.props.posts.map(function(post){
                return(
                    <div key={post.id}>
                        <div className="card">
                            <div>
                                <div className="titulo">{post.title}</div>
                                <div className="comentario">{post.body}</div>
                                <br/>
                                <img src="https://yt3.ggpht.com/a-/ACSszfFPy2UBWsUaiPHq-i3vsYQSs2mNATaow9U3mw=s900-mo-c-c0xffffffff-rj-k-no" className="imagen"></img>
                                <br/><br/>
                                    <div className="card-footer">
                                        <MySmallModal postpadre={post.id} postTitulo={post.title} postBody={post.body}/>     
                                    </div>
                            </div>
                        </div>
                        <br/>   
                    </div>   
                );
            });
        }

        render() {
            if(!this.props.posts.length){
                return  (
                    <div className="cargador">
                        <Loader type="Audio" color="white" height={300} width={300}/>     
                    </div>
                )
            }
            return (
                <div className="col-xs">
                    <div className="card-body">     
                        {this.renderPosts()}               
                    </div> 
                </div>
            )
        }
    }
export default Posts;