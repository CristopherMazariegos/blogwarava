import React, { Component } from 'react';
import { Collapse, 
  Navbar, 
  NavbarToggler, 
  NavbarBrand, 
  Nav, 
  NavItem, 
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

export class GlobalNav extends Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render() {
    return (
              <Navbar dark>
                <NavbarBrand href="#" className="mr-auto">BLOG WARAVA</NavbarBrand>
                <NavbarToggler onClick={this.toggleNavbar} className="mr-2"/>
                <Collapse isOpen={!this.state.collapsed} navbar>
                  <Nav navbar>
                    <NavItem>
                      <NavLink href="#">MUY PRONTO!</NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                              Opciones
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                              Opcion 1
                            </DropdownItem>
                              <DropdownItem divider/>
                            <DropdownItem>
                              Opcion 2
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                    <NavItem>
                      <NavLink href="#">MUY PRONTO!</NavLink>
                    </NavItem>                    
                  </Nav>
                </Collapse>
              </Navbar>  
    );
  }
}
export default GlobalNav;