import React, { Component } from 'react';
import './App.css';
import Posts from './components/Posts';
import api from './data/api.js';
import GlobalNav from './components/GlobalNav.js';

class App extends Component {
    constructor() {
        super();
        this.state = {
            posts:[]
        }    
    }

    componentDidMount(){
            api.posts.getList()
            .then(respuesta=> {
                this.setState({
                    posts:respuesta
                });
            })
            .catch(error=> {
                console.log(error)
            })
    }

    render() {
        return (
            <div className="App">
                <div>
                    <GlobalNav/>
                </div>
                <div className="container">
                    <Posts posts={this.state.posts} />
                </div>
            </div>
        );
      }
}
export default App;